var express = require('express');
var app = express();
var nodemailer = require('nodemailer');
var sesNodemailer = require('nodemailer-ses-transport');
var bodyParser = require('body-parser');

var jsonParser = bodyParser.json();
var urlencodedParser = bodyParser.urlencoded({ extended: false });
var urlActivation = 'http://www.taggerin.me/admin/#/access/activation/';
var urlRecovery = 'http://www.taggerin.me/admin/#/access/recovery/';
var urlImageCoffee= 'http://www.taggerin.me/admin/img/taggerincoffee.png';

var htmlFirstConfirmation = '<table align="center" bgcolor="#d5e4ed" width="100%" cellpadding="0" cellspacing="0" border="0"><tbody><tr><td align="center"><br><table cellpadding="0" cellspacing="0" border="0"><tbody><tr><td><table cellpadding="0" cellspacing="0" border="0" bgcolor="#ffffff" style="border-collapse:collapse;min-width:600px;" width="600"><tbody><tr><td valign="top" bgcolor="#ffffff"><table cellpadding="20" cellspacing="0" border="0" style="border-collapse:collapse;background-color:#ffffff;" bgcolor="#ffffff" width="100%"><tbody><tr><td><table width="100%" cellpadding="0" cellspacing="0" border="0" style="border-collapse:collapse;"><tbody><tr valign="top"><td valign="top" align="center" width="275">&nbsp;</td><td width="20" height="8"><b></b></td><td valign="top" width="275" style="line-height:130%;"><div style="text-align:right;color:rgb(127,127,127);"><br><span style="font-size:48px;color:rgb(127,127,127);line-height:130%;"><span style="color:rgb(127,127,127);font-family:Arial,Helvetica,sans-serif;line-height:130%;"><span style="font-size:28px;color:rgb(127,127,127);line-height:130%;"><strong style="color:rgb(127,127,127);">Bienvenido</strong></span></span></span></div></td></tr></tbody></table></td></tr></tbody></table><table cellpadding="0" cellspacing="0" border="0" width="100%" style="border-collapse:collapse;background-color:rgb(255,255,255);" bgcolor="#ffffff"><tbody><tr valign="top"><td valign="top" align="center"><img src="' + urlImageCoffee + '" alt="Texto mostrado cuando no se muestra la imagen" style="min-height:auto;vertical-align:top;width:600px;" width="600"></td></tr></tbody></table><table cellpadding="20" cellspacing="0" border="0" width="100%" style="border-collapse:collapse;background-color:rgb(46,164,219);" bgcolor="#2ea4db"><tbody><tr valign="top"><td valign="top" style="line-height:200%;color:rgb(0,0,0);"><div style="text-align:center;color:rgb(255,255,255);"><font face="arial, helvetica, sans-serif"><span style="font-size:36px;line-height:200%;"><strong>Confirmación de Registro</strong><br></span></font><span style="font-size:24px;font-family:arial,helvetica,sans-serif;font-weight:initial;line-height:200%;"><span style="font-size:16px;line-height:200%;">"Hemos mandado este correo de confirmación a tu cuenta, para poder autorizarlo te pedimos hacer click en el botón de abajo."<br></span><span style="font-size:10px;line-height:200%;">* Si no solicitaste un registro en Taggerin ignora este correo.</span></span></div></td></tr></tbody></table></td></tr><tr><td valign="top" bgcolor="#ffffff"><table cellpadding="20" cellspacing="0" border="0" width="100%" style="border-collapse:collapse;background-color:rgb(255,255,255);" bgcolor="#ffffff"><tbody><tr><td valign="top" align="center"><table cellpadding="0" cellspacing="0" border="0" align="center" style="width:100%;"><tbody><tr><td align="center"><a href="';
var htmlSecondConfirmation = '" style="background-color:rgb(47,170,222);border-radius:3px;color:#ffffff;display:inline-block;text-align:center;text-decoration:none;width:60%;" target="_blank">  <span style="padding:18px;display:block;">  <span style="display:block;font-family:Arial,Helvetica,sans-serif;color:rgb(255,255,255);text-decoration:none;">  <span style="font-size:26px;">Activar Cuenta</span>  </span></span></a></td></tr></tbody></table></td></tr></tbody></table></td></tr><tr><td valign="top" bgcolor="#ffffff"><table cellpadding="20" cellspacing="0" border="0" width="100%" style="border-collapse:collapse;background-color:rgb(229,229,229);" bgcolor="#e5e5e5"><tbody><tr valign="top"><td valign="top" style="line-height:130%;color:rgb(0,0,0);"><div style="color:rgb(102,102,102);text-align:center;">Taggerin, Zapopan, Jalisco.<br><a href="mailto:contacto@taggerin.me" target="_blank" class="">contacto@taggerin.me</a></div></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table><br></td></tr></tbody></table>';

var htmlFirstRecovery = '<table align="center" bgcolor="#d5e4ed" width="100%" cellpadding="0" cellspacing="0" border="0"><tbody><tr><td align="center"><br><table cellpadding="0" cellspacing="0" border="0"><tbody><tr><td><table cellpadding="0" cellspacing="0" border="0" bgcolor="#ffffff" style="border-collapse:collapse;min-width:600px;" width="600"><tbody><tr><td valign="top" bgcolor="#ffffff"><table cellpadding="20" cellspacing="0" border="0" style="border-collapse:collapse;background-color:#ffffff;" bgcolor="#ffffff" width="100%"><tbody><tr><td><table width="100%" cellpadding="0" cellspacing="0" border="0" style="border-collapse:collapse;"><tbody><tr valign="top"><td valign="top" align="center" width="275">&nbsp;</td><td width="20" height="8"><b></b></td><td valign="top" width="275" style="line-height:130%;"><div style="text-align:right;color:rgb(127,127,127);"><br><span style="font-size:48px;color:rgb(127,127,127);line-height:130%;"><span style="color:rgb(127,127,127);font-family:Arial,Helvetica,sans-serif;line-height:130%;"><span style="font-size:28px;color:rgb(127,127,127);line-height:130%;"><strong style="color:rgb(127,127,127);">Bienvenido</strong></span></span></span></div></td></tr></tbody></table></td></tr></tbody></table><table cellpadding="0" cellspacing="0" border="0" width="100%" style="border-collapse:collapse;background-color:rgb(255,255,255);" bgcolor="#ffffff"><tbody><tr valign="top"><td valign="top" align="center"><img src="' + urlImageCoffee + '" alt="Texto mostrado cuando no se muestra la imagen" style="min-height:auto;vertical-align:top;width:600px;" width="600"></td></tr></tbody></table><table cellpadding="20" cellspacing="0" border="0" width="100%" style="border-collapse:collapse;background-color:rgb(46,164,219);" bgcolor="#2ea4db"><tbody><tr valign="top"><td valign="top" style="line-height:200%;color:rgb(0,0,0);"><div style="text-align:center;color:rgb(255,255,255);"><font face="arial, helvetica, sans-serif"><span style="font-size:36px;line-height:200%;"><strong>Recuperación de la cuenta</strong><br></span></font><span style="font-size:24px;font-family:arial,helvetica,sans-serif;font-weight:initial;line-height:200%;"><span style="font-size:16px;line-height:200%;">"Hemos mandado este correo de recuperación de tu cuenta, para poder autorizarlo te pedimos hacer click en el botón de abajo."<br></span><span style="font-size:10px;line-height:200%;">* Si no solicitaste un registro en Taggerin ignora este correo.</span></span></div></td></tr></tbody></table></td></tr><tr><td valign="top" bgcolor="#ffffff"><table cellpadding="20" cellspacing="0" border="0" width="100%" style="border-collapse:collapse;background-color:rgb(255,255,255);" bgcolor="#ffffff"><tbody><tr><td valign="top" align="center"><table cellpadding="0" cellspacing="0" border="0" align="center" style="width:100%;"><tbody><tr><td align="center"><a href="';
var htmlSecondRecovery = '" style="background-color:rgb(47,170,222);border-radius:3px;color:#ffffff;display:inline-block;text-align:center;text-decoration:none;width:60%;" target="_blank">  <span style="padding:18px;display:block;">  <span style="display:block;font-family:Arial,Helvetica,sans-serif;color:rgb(255,255,255);text-decoration:none;">  <span style="font-size:26px;">Restablecer contraseña</span>  </span></span></a></td></tr></tbody></table></td></tr></tbody></table></td></tr><tr><td valign="top" bgcolor="#ffffff"><table cellpadding="20" cellspacing="0" border="0" width="100%" style="border-collapse:collapse;background-color:rgb(229,229,229);" bgcolor="#e5e5e5"><tbody><tr valign="top"><td valign="top" style="line-height:130%;color:rgb(0,0,0);"><div style="color:rgb(102,102,102);text-align:center;">Taggerin, Zapopan, Jalisco.<br><a href="mailto:contacto@taggerin.me" target="_blank" class="">contacto@taggerin.me</a></div></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table><br></td></tr></tbody></table>';

app.all('*', function(req,res,next){
        res.header("Access-Control-Allow-origin","*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        next();
});

var sesTransporter = nodemailer.createTransport(sesNodemailer({
   accessKeyId: 'AKIAIJYVL42NCB5YDMIQ',
   secretAccessKey: '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',
   region: 'us-east-1'
}));

app.post('/sendemailconfirm',jsonParser, function(req, res, next) {
  var templatehtml = htmlFirstConfirmation + urlActivation + req.body.token + htmlSecondConfirmation
  var mailOptions = {
    from: 'Taggerin <noreply@taggerin.me>',
          to: req.body.email,
          subject: 'Taggerin: Confirmación de registro',
          html: templatehtml
  };
   sesTransporter.sendMail(mailOptions, function(error, response){
   if(error){console.log(error);res.status(404).send('Sorry, we cannot find that');}
   else{res.status(202).send('Message successfully send to Taggerin');}
   });
});

app.post('/sendemailrecovery',jsonParser, function(req, res, next) {
  var templatehtml = htmlFirstRecovery + urlRecovery + req.body.token + htmlSecondRecovery
  var mailOptions = {
    from: 'Taggerin <noreply@taggerin.me>',
          to: req.body.email,
          subject: 'Taggerin: Recuperación de la Cuenta',
          html: templatehtml
  };
   sesTransporter.sendMail(mailOptions, function(error, response){
   if(error){console.log(error);res.status(404).send('Sorry, we cannot find that');}
   else{res.status(202).send('Message successfully send to Taggerin');}
   });
});

app.listen(80);

